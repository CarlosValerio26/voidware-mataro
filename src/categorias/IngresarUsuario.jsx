import React, { Component } from "react";
import { Link } from "react-router-dom";
import 'font-awesome/css/font-awesome.min.css';
import '../css/IngresarUsuario.css';

export default class Login extends Component {
    render() {
        return (
            <>
                <div class="container h-100">
                    <div class="d-flex justify-content-center h-100">
                        <div class="user_card">
                            <div class="d-flex justify-content-center">
                                <div class="brand_logo_container">
                                    <img src="https://f4.bcbits.com/img/0007828490_10.jpg" class="brand_logo" alt="Logo" />
                                </div>
                            </div>
                            <div class="d-flex justify-content-center form_container">
                                <form>
                                    <div class="input-group mb-3">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="fa fa-user"></i>
                                            </span>
                                        </div>
                                        <input type="text" name="" class="form-control input_user" value="" placeholder="Usuario" />
                                    </div>
                                    <div class="input-group mb-2">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-key"></i></span>
                                        </div>
                                        <input type="password" name="" class="form-control input_pass" value="" placeholder="Contraseña" />
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customControlInline" />
                                            <label class="custom-control-label" for="customControlInline">Recuérdame</label>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center mt-3 login_container">
                                        <button type="button" name="button" class="btn login_btn">Ingresar</button>
                                    </div>
                                </form>
                            </div>

                            <div class="mt-4">
                                <div class="d-flex justify-content-center links">
                                    ¿No tienes cuenta? <Link to="/categorias/registro-usuarios"><a href="#" class="ml-2">Registrate</a> </Link>
                                </div>
                                <div class="d-flex justify-content-center links">
                                <Link to="/categorias/recuperar-contraseña"><a href="#" class="ml-2">¿Has olvidado tu contraseña?</a> </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </>
        );
    }
}