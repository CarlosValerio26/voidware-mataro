import React, { Component } from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { Container, Row, Col } from 'reactstrap';
import { Link, Switch, Route } from "react-router-dom";
import '../App.css';
// dsdds
import Carousel from 'react-bootstrap/Carousel';
import Image from 'react-bootstrap/Image';


export default class Login extends Component {
    render() {
        return (
            <>
                <Navbar bg="light" expand="lg">
                    <Container className="container-brand">

                        <Row>

                            <Col>
                                <Image className="img-brand" src="https://f4.bcbits.com/img/0007828490_10.jpg" roundedCircle />
                                <Navbar> <Link to="/categorias/ingresar-usuarios" className="link-brand">VOIDWARE</Link></Navbar>
                            </Col>
                        </Row>
                    </Container>
                </Navbar>

                <Navbar bg="dark" expand="lg" variant="dark">
                    <Container>
                        <Row>
                            <Col>
                                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                                <Navbar.Collapse id="basic-navbar-nav">
                                    <Nav className="mr-auto">
                                        <Nav.Link href="#home">INICIO</Nav.Link>
                                        <NavDropdown title="QUE HACEMOS" id="basic-nav-dropdown">
                                            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                                            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                                            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                                        </NavDropdown>                                        <Nav.Link href="#link">SOBRE NOSOTROS</Nav.Link>
                                        <Nav.Link href="#link">CONTÁCTANOS</Nav.Link>
                                    </Nav>
                                </Navbar.Collapse>
                            </Col>
                            {/* 
                            <Col>
                                <Nav className="mr-auto">
                                    <Nav.Link href="#home">USUARIO</Nav.Link>
                                    <Nav.Link href="#link">REGISTRAR</Nav.Link>
                                </Nav>
                            </Col> */}
                        </Row>
                    </Container>
                </Navbar>

                <Carousel fade>
                    <Carousel.Item>
                        <img
                            className="d-block w-100 h-25"
                            src="https://f4.bcbits.com/img/0007828490_10.jpg"
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100 h-25"
                            src="https://f4.bcbits.com/img/0007828490_10.jpg"
                            alt="Second slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100 h-25"
                            src="https://f4.bcbits.com/img/0007828490_10.jpg"
                            alt="Third slide"
                        />
                    </Carousel.Item>
                </Carousel>


                {/* PIE DE PAGINA */}
                <Container className="container-footer">
                    <Row>
                        <Col xs={6} md={4}>
                            <Image className="img-brand" src="https://f4.bcbits.com/img/0007828490_10.jpg" roundedCircle />
                            <Navbar> <Link className="link-brand" to="/">VOIDWARE</Link></Navbar>
                            <Nav.Link href="#link">CONTÁCTANOS</Nav.Link>
                            <Nav.Link href="#link">DIRECCIÓN</Nav.Link>
                            <Nav.Link href="#link">Carrer del canigò</Nav.Link>

                        </Col>
                        <Col xs={6} md={4}>
                            <h5>Informacion</h5>
                            <Nav.Link href="#link">Sobre Nosotros</Nav.Link>
                            <Nav.Link href="#link">Contáctanos</Nav.Link>
                            <Nav.Link href="#link">Política de Privacidad</Nav.Link>
                            <Nav.Link href="#link">Términos y Condiciones</Nav.Link>
                        </Col>

                        <Col>
                        </Col>
                    </Row>
                </Container>

                <Container>
                    <Row>
                        <Col>
                            <p>&copy; 2021–actual Company, Inc. &middot; <a href="#">Privacidad</a> &middot; <a href="#">Terminos</a></p>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}