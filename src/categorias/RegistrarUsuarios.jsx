import React, { Component } from "react";
import { Link } from "react-router-dom";
import 'font-awesome/css/font-awesome.min.css';

import '../css/RegistrarUsuario.css';

export default class SignUp extends Component {
    render() {
        return (
            <div class="container">

                <div class="card bg-light">
                    <article class="card-body mx-auto" >
                        <h4 class="card-title mt-3 text-center">Crear Cuenta</h4>
                        <p class="text-center">Empiece con su cuenta gratuita</p>
                        <form>
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                </div>
                                <input name="" class="form-control" placeholder="Usuario" type="text" />
                            </div>
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                                </div>
                                <input name="" class="form-control" placeholder="Correo electrónico" type="email" />
                            </div>    <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                </div>
                                <select class="custom-select">
                                    <option selected="">+34</option>
                                    <option value="1">+972</option>
                                    <option value="2">+198</option>
                                    <option value="3">+701</option>
                                </select>
                                <input name="" class="form-control" placeholder="Teléfono" type="text" />
                            </div>
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                                </div>
                                <input class="form-control" placeholder="Crear contraseña" type="password" />
                            </div>
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
                                </div>
                                <input class="form-control" placeholder="Repite contraseña" type="password" />
                            </div>
                            <div class="form-group button">
                                <button type="submit" class="btn btn-primary btn-block"> Crear Cuenta  </button>
                            </div>
                            <p class="text-center">¿Tienes una cuenta? <Link to="/categorias/ingresar-usuarios"><a href="#" class="ml-2">Ingresa</a> </Link> </p>
                        </form>
                    </article>
                </div>
            </div>

        );
    }
}