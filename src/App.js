import React, { useState, useEffect } from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './App.css';
// saass
import Inicio from './categorias/Inicio'
import GoogleMap from './categorias/GoogleMap'
import RegistrarUsuarios from './categorias/RegistrarUsuarios'
import IngresarUsuario from './categorias/IngresarUsuario'
import RecuperarContraseña from './categorias/RecuperarContraseña'
import SobreNosotros from './categorias/SobreNosotros'
// import Maps from "./modelos/Maps";
const App = () => {
  // const mapa = useState();
  // fdf
  return (
    // <div className="App">
    //   <Maps />
    //  </div>
    <BrowserRouter>
      <Container >
        <Row>
          <Col>
            <ul className="list-unstyled menu">
              {/* <li> <Link className="link" to="/">Inicio</Link> </li>
              <li> <Link className="link" to="/categorias/googlemaps">Mapa</Link> </li>
              <li> <Link className="link" to="/categorias/registro-usuarios">Registrarse</Link> </li>
              <li> <Link className="link" to="/categorias/ingresar-usuarios">Ingresar</Link> </li>
              <li> <Link className="link" to="/categorias/recuperar-contraseña">Recuperar contraseña</Link> </li> */}
              <li> <Link className="link" to="/categorias/sobre-nosotros">Sobre nosotros</Link> </li>
            </ul>
          </Col>
        </Row>
        <Switch>
          <Route exact path="/" component={Inicio} />
          <Route path="/categorias/googlemaps" render={() => <GoogleMap />} />
          <Route path="/categorias/registro-usuarios" render={() => <RegistrarUsuarios />} />
          <Route path="/categorias/ingresar-usuarios" render={() => <IngresarUsuario />} />
          <Route path="/categorias/recuperar-contraseña" render={() => <RecuperarContraseña />} />
          <Route path="/categorias/sobre-nosotros" render={() => <SobreNosotros />} />
        </Switch>
      </Container>
    </BrowserRouter>
  );
}

export default App;
