import React, { Component } from "react";
import '../css/RecuperarContraseña.css'
// ddf
export default class Login extends Component {

    render() {
        return (
            <div class="form-gap">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-md-offset-4">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="text-center">
                                        <h3><i class="fa fa-lock fa-4x"></i></h3>
                                        <h2 class="text-center">¿Has olvidado tu contraseña?</h2>
                                        <p>Aqui podrás recuperarla.</p>
                                        <div class="panel-body">
                                            <form id="register-form" role="form" autocomplete="off" class="form" method="post">

                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                        <input id="email" name="email" placeholder="Correo electronico" class="form-control" type="email" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Recuperar Contraseña" type="submit" />
                                                </div>

                                                <input type="hidden" class="hide" name="token" id="token" value="" />
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}